import React, {Component} from 'react'
import './burgerBuilder.css'
import Menu from "./components/menu/menu";
import Ingredients from "./components/ingredients/ingredients";

class Burger extends Component {
	
	state = {
		Totalprice: 20,
		ingredients: [
			{name: 'Salad',price: 5, amount: 0, disabled:true},
			{name: 'Bacon',price: 30, amount: 0, disabled:true},
			{name: 'Cheese',price: 20, amount: 0, disabled:true},
			{name: 'Meat',price: 50, amount: 0, disabled:true}
		],
	};
	
	addIngredient = (name) => {
		const ingredients = [...this.state.ingredients];
		const index = ingredients.findIndex(p => p.name === name);
		ingredients[index].amount++;
		ingredients[index].disabled = false;
		let Totalprice = this.state.Totalprice;
		Totalprice += ingredients[index].price;

		this.setState({ingredients,Totalprice});


    };
	
	removeIngredient = (name) => {

		const ingredients = [...this.state.ingredients];
		const index = ingredients.findIndex(p => p.name === name);
        let Totalprice = this.state.Totalprice;
        if((ingredients[index].amount === 0)){
			ingredients[index].disabled = true;
        }else {
            ingredients[index].amount--;
            Totalprice -= ingredients[index].price;
		}
		this.setState({ingredients,Totalprice});


	};
	
	render() {
		return (
			<div className="BurgerBuilder">
				<div className="Burger">
					<div className="BreadTop">
						<div className="Seeds1"></div>
						<div className="Seeds2"></div>
					</div>
					<Ingredients ingredients={this.state.ingredients}/>
					<div className="BreadBottom"></div>
				</div>
				<Menu
					totalPrice={this.state.Totalprice}
					add={this.addIngredient}
					ingredients={this.state.ingredients}
					remove={this.removeIngredient}

				/>
			</div>
		)
	}
}

export default Burger;