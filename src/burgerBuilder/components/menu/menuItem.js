import React from 'react'

const MenuItem = props => {

	return (
		<div className="row">
			<span>{props.name}</span>
			<button onClick={() => props.remove(props.name)} className="less" disabled={props.disabled}>Less</button>
			<button onClick={() => props.add(props.name)} className="more">More</button>
		</div>
	)
};

export default MenuItem;